#!/bin/bash

export ARCH=arm
#export CROSS_COMPILE=$HOME/asus/cm12/prebuilts/gcc/linux-x86/arm/arm-eabi-4.8/bin/arm-eabi-

CORE_COUNT=`grep processor /proc/cpuinfo|wc -l`
if [ -z "$OUT_DIR" ]; then
export OUT_DIR=out
fi

mkdir $OUT_DIR

make -C $(pwd) O=$OUT_DIR msm8916_sec_defconfig VARIANT_DEFCONFIG=$TARGET_DEFCONFIG SELINUX_DEFCONFIG=selinux_defconfig
make -C $(pwd) -j$CORE_COUNT O=$OUT_DIR

chmod 755 tools/dtbToolCM
tools/dtbToolCM -3 -s 2048 -o $OUT_DIR/arch/arm/boot/dt.img -p $OUT_DIR/scripts/dtc/ $OUT_DIR/arch/arm/boot/dts/
